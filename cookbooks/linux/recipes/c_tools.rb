package ['build-essential', 'valgrind'] do
  action :upgrade
end

execute 'radare_setup' do
  command "#{ENV['HOME']}/radare/sys/user.sh"
  creates "#{ENV['HOME']}/bin/r2"
  action :nothing
end

git 'radare' do
  repository 'https://github.com/radare/radare2.git'
  destination "#{ENV['HOME']}/radare"
  revision 'master'
  notifies :run, 'execute[radare_setup]', :immediately
end

